### Запуск сервера

```kotlin
./gradlew bootRun
```

### URL сервера
```kotlin
http://localhost:8888
```

### Консоль БД
```kotlin
http://localhost:8888/h2-console
```

