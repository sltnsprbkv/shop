package edu.hse.shop.service

import edu.hse.shop.model.Product
import edu.hse.shop.repository.ProductRepository
import org.springframework.stereotype.Service
import java.util.*

@Service
class ProductService(private val productRepository: ProductRepository) {

    fun getAllProducts(): List<Product> = productRepository.findAll()

    fun getProductById(id: UUID): Product? = productRepository.findById(id).orElse(null)

    fun addProduct(product: Product): Product = productRepository.save(product)

    fun updateProduct(id: UUID, updatedProduct: Product): Product? {
        return if (productRepository.existsById(id)) {
            productRepository.save(updatedProduct.copy(id = id))
        } else {
            null
        }
    }

    fun deleteProduct(id: UUID) {
        productRepository.deleteById(id)
    }
}