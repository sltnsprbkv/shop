package edu.hse.shop.model

import jakarta.persistence.Entity
import jakarta.persistence.Id
import java.util.UUID

@Entity
data class Product(
    @Id
    val id: UUID = UUID.randomUUID(),
    val imgSrc: String,
    val category: String,
    val name: String,
    val price: Double,
    var isAddCart: Boolean,
    val content: String
)
