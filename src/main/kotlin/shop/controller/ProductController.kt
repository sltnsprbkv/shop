package edu.hse.shop.controller

import edu.hse.shop.model.Product
import edu.hse.shop.service.ProductService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/products")
class ProductController(private val productService: ProductService) {

    @GetMapping
    fun getAllProducts(): List<Product> = productService.getAllProducts()

    @GetMapping("/{id}")
    fun getProductById(@PathVariable id: UUID): ResponseEntity<Product> {
        val product = productService.getProductById(id)
        return if (product != null) {
            ResponseEntity.ok(product)
        } else {
            ResponseEntity.notFound().build()
        }
    }

    @PatchMapping("{id}")
    fun addProduct(@PathVariable id: String): ResponseEntity<Product> {
        val product = productService.getProductById(UUID.fromString(id))!!
        product.isAddCart = !product.isAddCart
        val newProduct = productService.addProduct(product)
        return ResponseEntity.status(HttpStatus.OK).body(newProduct)
    }

    @PutMapping("/{id}")
    fun updateProduct(@PathVariable id: UUID, @RequestBody updatedProduct: Product): ResponseEntity<Product> {
        val updated = productService.updateProduct(id, updatedProduct)
        return if (updated != null) {
            ResponseEntity.ok(updated)
        } else {
            ResponseEntity.notFound().build()
        }
    }

    @DeleteMapping("/{id}")
    fun deleteProduct(@PathVariable id: UUID): ResponseEntity<Void> {
        productService.deleteProduct(id)
        return ResponseEntity.noContent().build()
    }
}