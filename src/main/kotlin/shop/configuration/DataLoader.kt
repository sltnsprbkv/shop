package edu.hse.shop.configuration

import edu.hse.shop.model.Product
import edu.hse.shop.repository.ProductRepository
import org.springframework.boot.CommandLineRunner
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.util.*

@Configuration
class DataLoader {

    @Bean
    fun initDatabase(productRepository: ProductRepository): CommandLineRunner {
        return CommandLineRunner {
            val products = listOf(
                Product(
                    UUID.fromString("b020de1e-6b1a-4b7e-b64d-3b90d8c83bfa"),
                    "https://plus.unsplash.com/premium_photo-1681305758171-f28f43aa0dc7?q=80&w=2129&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
                    "Электроника",
                    "Смартфон",
                    499.99,
                    false,
                    "Смартфон высокой производительности с дисплеем 6.5 дюйма, 128 ГБ памяти и камерой 48 МП."
                ),
                Product(
                    UUID.fromString("9f0af03b-9e06-4968-b271-59a9e244dc79"),
                    "https://images.unsplash.com/photo-1523381294911-8d3cead13475?q=80&w=2070&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
                    "Одежда",
                    "Футболка",
                    19.99,
                    false,
                    "Удобная хлопковая футболка, доступная в разных цветах и размерах. Идеально подходит для повседневной носки."
                ),
                Product(
                    UUID.fromString("55efb7f0-dc89-4a4b-8ae0-88a7d740cfe2"),
                    "https://images.unsplash.com/photo-1593369196682-6d8ec9ff3ae0?q=80&w=2070&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
                    "Товары для дома",
                    "Кофеварка",
                    79.99,
                    false,
                    "Приготовьте ваш любимый кофе с помощью этой простой в использовании кофеварки. Оснащена программируемыми настройками и стильным дизайном."
                ),
                Product(
                    UUID.fromString("0a62e5e1-2c96-4f7d-82d2-3783dbf571f7"),
                    "https://images.unsplash.com/photo-1495446815901-a7297e633e8d?q=80&w=2070&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
                    "Книги",
                    "Роман",
                    12.99,
                    false,
                    "Захватывающий роман с интригующим сюжетом и разработанными персонажами. Идеален для любителей художественной литературы."
                ),
                Product(
                    UUID.fromString("5be39b4a-dba8-4ac3-a3ab-38dcf4c11c9f"),
                    "https://images.unsplash.com/photo-1505740420928-5e560c06d30e?q=80&w=2070&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
                    "Электроника",
                    "Наушники",
                    99.99,
                    false,
                    "Беспроводные наушники высокой точности с шумоподавлением. Удобный дизайн и долгий срок службы батареи."
                ),
                Product(
                    UUID.fromString("a61998e6-cc15-4c82-bfd2-0a91e081e2f2"),
                    "https://images.unsplash.com/photo-1523293182086-7651a899d37f?q=80&w=2070&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
                    "Красота",
                    "Парфюм",
                    49.99,
                    false,
                    "Элегантный и стойкий аромат. Поставляется в красиво оформленном флаконе, идеально подходящем для подарков."
                ),
                Product(
                    UUID.fromString("4cc6c672-41f8-4b15-b67d-bc08367d5d48"),
                    "https://images.unsplash.com/photo-1469395446868-fb6a048d5ca3?q=80&w=1933&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
                    "Спорт",
                    "Кроссовки для бега",
                    79.99,
                    false,
                    "Прочные и легкие кроссовки для бега. Обеспечивают отличную поддержку и комфорт для длительных пробежек."
                ),

                Product(
                    UUID.fromString("7e018122-81b0-4a7c-9b49-0500d0c08361"),
                    "https://images.unsplash.com/photo-1580460848618-b1a43e74519b?q=80&w=2070&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
                    "Товары для дома",
                    "Декор. подушка",
                    24.99,
                    false,
                    "Мягкая и стильная декоративная подушка, идеально подходит для создания уютной обстановки в вашем доме."
                ),
                Product(
                    UUID.fromString("efb8d56f-5d9e-4c16-b93e-fd215a92d30a"),
                    "https://images.unsplash.com/photo-1593369196682-6d8ec9ff3ae0?q=80&w=2070&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
                    "Товары для дома",
                    "Кофеварка",
                    79.99,
                    false,
                    "Удобная кофеварка с легко очищаемым дизайном. Наслаждайтесь свежей чашечкой кофе каждое утро."
                ),
                Product(
                    UUID.fromString("9ff35f65-6f6c-40f0-bd70-c31fa554d282"),
                    "https://plus.unsplash.com/premium_photo-1661454543510-79bf55705670?q=80&w=1931&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
                    "Красота",
                    "Оч. сред. для лица",
                    14.99,
                    false,
                    "Нежное очищающее средство для лица, которое удаляет загрязнения и оставляет кожу освеженной и чистой."
                ),
                Product(
                    UUID.fromString("d46f5071-7e52-4dbb-a3c5-d6c7e128e239"),
                    "https://plus.unsplash.com/premium_photo-1667739346017-fbc9cd35d666?q=80&w=2069&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
                    "Спорт",
                    "Коврик для йоги",
                    29.99,
                    false,
                    "Коврик для йоги с антискользящим покрытием и амортизирующей поддержкой, идеален для йоги и других упражнений на полу."
                ),
                Product(
                    UUID.fromString("f87a700e-2a98-4f78-b421-fdd47b96d5a5"),
                    "https://images.unsplash.com/photo-1516979187457-637abb4f9353?q=80&w=2070&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90о-pYWdlfHx8fGVufDB8fHx8fA%3D%3D",
                    "Книги",
                    "Биография",
                    17.99,
                    false,
                    "Вдохновляющая биография известного человека. Предоставляет глубокое понимание их жизни и достижений."
                ),
                Product(
                    UUID.fromString("16394d70-0b0a-457e-bc8c-d8a47d63f1d4"),
                    "https://images.unsplash.com/photo-1589003077984-894e133dabab?q=80&w=1974&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaGото-pYWdlfHx8fGVufDB8fHx8fA%3D%3D",
                    "Электроника",
                    "Беспроводная колонка",
                    149.99,
                    false,
                    "Портативная беспроводная колонка с отличным качеством звука и компактным дизайном. Идеальна для прослушивания в дороге."
                ),

                Product(
                    UUID.fromString("1f4b8a4f-dbf5-4d02-ae57-39c4a0843ab9"),
                    "https://images.unsplash.com/photo-1618932260643-eee4a2f652a6?q=80&w=1980&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaGото-pYWdlfHx8fGVufDB8fHx8fA%3D%3D",
                    "Одежда",
                    "Платье",
                    59.99,
                    false,
                    "Элегантное платье, подходящее для различных случаев. Сделано из высококачественной ткани и с лестной посадкой."
                ),
                Product(
                    UUID.fromString("be3275c7-cf86-49c5-8165-4856c78a2bc0"),
                    "https://plus.unsplash.com/premium_photo-1672868761626-0a5d85d3626b?q=80&w=2070&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
                    "Дом",
                    "Плед",
                    39.99,
                    false,
                    "Уютный и мягкий плед, идеально подходит для тепла в прохладные вечера. Доступен в различных цветах."
                ),
                Product(
                    UUID.fromString("22b0d5f6-8f23-4e90-a1c4-3f3f73c54fd0"),
                    "https://images.unsplash.com/photo-1625093742435-6fa192b6fb10?q=80&w=2089&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
                    "Красота",
                    "Помада",
                    9.99,
                    false,
                    "Яркая помада с долговременным цветом. Обеспечивает гладкое и увлажненное покрытие."
                ),
                Product(
                    UUID.fromString("7e018122-81b0-4a7c-9b49-0500d0c08361"),
                    "https://images.unsplash.com/photo-1580460848618-b1a43e74519b?q=80&w=2070&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
                    "Товары для дома",
                    "Декор. подушка",
                    24.99,
                    false,
                    "Мягкая и стильная декоративная подушка, идеально подходит для создания уютной обстановки в вашем доме."
                )
            )
            productRepository.saveAll(products)
        }
    }
}